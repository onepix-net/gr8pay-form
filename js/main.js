document.addEventListener('DOMContentLoaded', function () {
	var amountField = document.getElementById('amount');
	var amountTotal = document.querySelector('.total');
	var amountTotalValue = amountTotal.textContent;

	Inputmask({
		alias: 'numeric',
		groupSeparator: ',',
		digits: 2,
		digitsOptional: false,
		prefix: '£',
		placeholder: '0',
		rightAlign: false,
		definitions: {
			0: {
				validator: '[0-9\uFF11-\uFF19]',
			},
		},
	}).mask(amountField);

	amountField.addEventListener('input', function () {
		amountTotal.textContent = this.value;
		if (this.value == '') amountTotal.textContent = amountTotalValue;
	});
});

function generateLink(){
	let user_id = document.getElementById('user_id').value;
	let amount = document.getElementById('amount').value;
	let description = document.getElementById('description').value;
	let captcha = grecaptcha.getResponse();

	let formData = new FormData();
	formData.append('user_id', user_id);
	formData.append('amount', amount);
	formData.append('description', description);
	formData.append('captcha', captcha);

	let xmlHttp = new XMLHttpRequest();
	xmlHttp.onreadystatechange = function()
	{
		if(xmlHttp.readyState == 4 && xmlHttp.status == 200)
		{
			let short_link_wrap = document.getElementById('short-link-wrap');
			let captcha_error_wrap = document.getElementById('recaptchaError');
			let response = JSON.parse(xmlHttp.responseText);

			if( response.captcha ){
				captcha_error_wrap.innerHTML = '';
				fetch('https://api-ssl.bitly.com/v4/shorten', {
					method: 'POST',
					headers: {
						'Authorization': 'Bearer bcfee761fed81e9600f537a3eea161b413e87512',
						'Content-Type': 'application/json'
					},
					body: JSON.stringify({ "long_url": response.html, "domain": "bit.ly" })
				})
				.then((response) => {
					return response.json();
				})
				.then((data) => {
					short_link_wrap.innerHTML = '<a id="short-link" target="_blank" style="text-align: center;" href="' + data.link + '">' + data.link + '</a><a href="#" id="copy-short-link"></a>';
				});
				
				grecaptcha.reset();
				document.getElementById("short-link-wrap").scrollIntoView({block: "center", behavior: "smooth"});
			} else {
				short_link_wrap.innerHTML = '';
				captcha_error_wrap.innerHTML = response.html;
			}
		}
	}
	xmlHttp.open("post", "generate-payment-link.php");
	xmlHttp.send(formData);
};

document.addEventListener('click', function(e) {
	if( e.target && e.target.id == 'copy-short-link' ){
		e.preventDefault();
		let link = document.getElementById('short-link');
		let href = link.href;
		let inputc = document.body.appendChild(document.createElement("input"));
		inputc.value = href;
		inputc.focus();
		inputc.select();
		document.execCommand('copy');
		inputc.parentNode.removeChild(inputc);

		let copy_button = document.getElementById('copy-short-link');
		copy_button.classList.add("message-copied");

		let remove_copied_class = setTimeout(function (){
			copy_button.classList.remove("message-copied");
		}, 3000);
	}
});