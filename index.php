<?php

require_once "functions.php";

if (isset($_GET["g-recaptcha-response"])) {
    $response = checkCaptcha( $_GET["g-recaptcha-response"]	);
}

if (isset($_GET['user_id']) && isset($_GET['amount']) && isset($_GET['description'])) {
	if ( $response != null && $response->success ) {
		$redirect_url = generatePaymentURL( $_GET['user_id'], $_GET['amount'], $_GET['description'] );
		header( 'Location:' . $redirect_url );
		$reCaptcha_message = '';
	} else {
		$reCaptcha_message = 'Please, complete the captcha';
	}
} else {
	$reCaptcha_message = '';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form</title>
    <link rel="stylesheet" href="./css/main.css">
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
</head>
<body>

<div class="content">

    <div class="form-wrapper">
        <form class="payment-form" method="get">

            <h3>Order details</h3>

            <div class="form-row">
                <label for="name">User ID</label>
                <input type="text" name="user_id" id="user_id" placeholder="User ID" required value="<?php echo $_GET['user_id']; ?>">
            </div>

            <div class="form-row">
                <label for="amount">Amount</label>
                <input inputmode="numeric" name="amount" id="amount" placeholder="£0.00" required value="<?php echo $_GET['amount']; ?>">
            </div>

            <div class="form-row">
                <label for="amount">Purchase Description</label>
                <textarea inputmode="text" name="description" id="description" placeholder="Purchase Description" required><?php echo $_GET['description']; ?></textarea>
            </div>

            <div class="form-total">
                <span>Total <span class="total"><?php echo ($_GET['amount']) ? $_GET['amount']: '£0'; ?></span></span>
            </div>
            <div class="g-recaptcha" data-sitekey="6LfQL4YaAAAAALCH9tDOEHjA28OIYEYiX0p01Mt4"></div>
            <div class="text-danger" id="recaptchaError"><?php echo $reCaptcha_message; ?></div>
            <div class="form-buttons">
                <input type="button" id="generate-payment-link" value="Link" onclick="generateLink();">
                <input type="submit" value="Pay">
            </div>

	        <div id="short-link-wrap"></div>

            <a href="#">Terms & Conditions</a>

            <ul>
                <li><img src="./img/visa.svg" alt=""></li>
                <li><img src="./img/mastercard.svg" alt=""></li>
                <li><img src="./img/american-express.svg" alt="" style="width: 70%;"></li>
            </ul>

        </form>

        <a href="#" class="site-logo"><img src="./img/logo.png" alt="Gr8Pay"></a>

    </div>

</div>

<script src="./libs/inputmask/inputmask.min.js"></script>
<script src="./js/main.js"></script>
</body>
</html>
