<?php

	require_once "functions.php";

	if( isset($_POST['user_id']) && $_POST['user_id'] != "" && isset($_POST['amount']) && $_POST['amount'] != "" && isset($_POST['description']) && $_POST['description'] != "" ){
		$user_id        = $_POST['user_id'];
		$amount         = $_POST['amount'];
		$description    = $_POST['description'];
		$captcha        = $_POST['captcha'];

		$captcha_response = checkCaptcha( $captcha	);

		if ( $captcha_response != null && $captcha_response->success ) {
			$payment_link       = generatePaymentURL( $user_id, $amount, $description );
			$response = array(
				'captcha'       => true,
				'html'          => $payment_link,
			);

		} else {

			$response = array(
				'captcha'           => false,
				'html'              => 'Please, complete the captcha',
			);

		}

	} else {

		$response = array(
			'captcha'   => false,
			'html'      => 'Fill all fields, please'
		);
	}

	$json = json_encode($response);
	echo $json;