<?php
	require_once "recaptchalib.php";

	function generatePaymentURL( $user_id, $amount, $description ){
		$url = 'https://uiservices.coriunder.cloud/hosted/';
		$data['merchantID'] = '8718794';
		$data['url_redirect'] = 'https://www.google.com/';
		$data['notification_url'] = 'https://www.google.com/';
		$data['trans_installments'] = '1';
		$data['trans_type'] = '0';
		$data['amount'] = str_replace('£', '', $amount);
		$data['trans_currency'] = 'GBP';
		$data['trans_comment'] = $user_id;
		$data['disp_payFor'] = $description;
		$data['disp_lng'] = 'en-US';
		$data['disp_lngList'] = 'All';
		$data['personal_hash'] = 'O7DY96820B';
		$data['name'] = $_GET['name'];

		$signature = do_signature($data);

		$valuesString = '&merchantID=' . $data['merchantID'];
		$valuesString .= '&url_redirect=' . $data['url_redirect'];
		$valuesString .= '&notification_url=' . $data['notification_url'];
		$valuesString .= '&trans_installments=' . $data['trans_installments'];
		$valuesString .= '&trans_type=' . $data['trans_type'];
		$valuesString .= '&trans_amount=' . $data['amount'];
		$valuesString .= '&trans_currency=' . $data['trans_currency'];
		$valuesString .= '&trans_comment=' . $data['trans_comment'];
		//$valuesString .= '&trans_refNum=' . urlencode($order_id);
		$valuesString .= '&disp_payFor=' . $data['disp_payFor'];
		$valuesString .= '&disp_lng=' . $data['disp_lng'];
		$valuesString .= '&disp_lngList=' . $data['disp_lngList'];
		$valuesString .= '&client_Name=' . $data['name'];
		$valuesString .= '&signature=' . $signature;

		$redirect_url = $url . '?' . $valuesString;
		return $redirect_url;
	}

	function do_signature($data)
	{
		// 1.Concatenate the required parameters values.
		// and making sure it is in correct order!
		$valuesString = $data['merchantID'];
		$valuesString .= $data['url_redirect'];
		$valuesString .= $data['notification_url'];
		$valuesString .= $data['trans_installments'];
		$valuesString .= $data['trans_type'];
		$valuesString .= $data['amount'];
		$valuesString .= $data['trans_currency'];
		$valuesString .= $data['trans_comment'];
		//$valuesString .= $order_id;
		$valuesString .= $data['disp_payFor'];
		$valuesString .= $data['disp_lng'];
		$valuesString .= $data['disp_lngList'];
		$valuesString .= $data['name'];
		// 2.Add the Personal Hash Key to the end of the concatenated string.
		$valuesString .= $data['personal_hash'];

		// 3.Apply SHA256 hash to the string.
		$signature_hash = hash('sha256', $valuesString, true);

		// 4.Convert the hash result to Base64.
		$signature_base64 = base64_encode($signature_hash);

		// 5.URL encode the Base64.
		$signature = urlencode($signature_base64);

		return $signature;
	}

	function checkCaptcha( $captcha ){
		$secret = "6LfQL4YaAAAAABx5Vc1dNoRbyIK72sXT_Rz_2m0M";
		$response = null;
		$reCaptcha = new ReCaptcha($secret);

		$response = $reCaptcha->verifyResponse(
			$_SERVER["REMOTE_ADDR"],
			$captcha
		);

		return $response;
	}